﻿using FluentAssertions;
using System.Linq;
using System.Xml.Linq;

public class Program
{
    public static void Main(string[] args)
    {
        Trie trie = new Trie();
        trie.Insert("apple");
        trie.Insert("fixes");

        Console.WriteLine("Проверка префиксного дерева");

        trie.Search("app").Should().BeFalse();
        trie.StartsWith("app").Should().BeTrue();
        trie.Search("apple").Should().BeTrue();
        trie.StartsWith("apple").Should().BeTrue();
        trie.Search("fix").Should().BeFalse();
        trie.StartsWith("fix").Should().BeTrue();
        trie.Search("fixes").Should().BeTrue();
        trie.StartsWith("fixes").Should().BeTrue();

        Console.WriteLine("Проверка пройдена");

        Console.ReadLine();
    }
}


public class Node
{
    private const int CharSize = 128;

    public Node[] Children;
    public bool IsEndOfWord;

    public Node()
    {
        Children = new Node[CharSize];
        IsEndOfWord = false;
    }
}

public static class Extensions
{
    public static bool Contains(this Node[] children, char c)
    {
        return children[c] != null;
    }
}

public class Trie
{
    Node root;

    public Trie()
    {
        root = new Node();
    }

    public void Insert(string word)
    {
        Node current = root;

        foreach (char ch in word)
        {
            if (!current.Children.Contains(ch))
            {
                current.Children[ch] = new Node();
            }
            current = current.Children[ch];
        }
        current.IsEndOfWord = true;
    }

    public bool Search(string word)
    {
        Node current = root;
        foreach (var ch in word)
        {
            if (!current.Children.Contains(ch))
            {
                return false;
            }
            current = current.Children[ch];
        }
        return current.IsEndOfWord;
    }

    public bool StartsWith(string prefix)
    {
        Node current = root;
        foreach (var ch in prefix)
        {
            if (!current.Children.Contains(ch))
            {
                return false;
            }
            current = current.Children[ch];
        }
        return true;
    }
}

